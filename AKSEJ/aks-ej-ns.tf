# Configure the Microsoft Azure Provider1
provider "azurerm" {
    # The "feature" block is required for AzureRM provider 2.x. 
    # If you're using version 1.x, the "features" block is not allowed.
    version = "~>2.0"
    features {}

# variable definitions

variable "resource_group_name" {
  description = "Azure Resource Group Name"
}

variable "resource_group_id" {
  description = "Azure Resource Group Id"
}

variable "azure_location" {
  description = "Azure Location, e.g. North Europe"
  default = "Central US"
}

variable "tags" {
  description = "Map of tags to apply to all resources"
  type = map(string)
}

variable "aks_name" {
  description = "AKS Name"
}

variable "aks_dns_prefix" {
  description = "DNS prefix"
}

variable "aks_node_count" {
  description = "DNS prefix"
  default = 1
}

variable "aks_node_sku" {
  description = "SKU of the VMs running in the default node pool e.g. Standard_D2_v2"
  default = "Standard_D2_v2"
}

variable "aks_vnet_subnet_id" {
  description = "GUID of the subnet to create the nodes in"
}

variable "aks_client_id" {
  description = "GUID of the AKS Service Principal"
}

variable "aks_client_secret" {
  description = "secret of the AKS Service Principal"
}

variable "aks_dns_service_ip" {
  description = "IP for the DNS Service"
}

variable "aks_docker_bridge_cidr" {
  description = "CIDR for the docker bridge"
}

variable "aks_service_cidr" {
  description = "CIDR for the AKS service"
}

variable "aks_node_resource_group_name" {
  description = "Name of the resource group the node pool will exist"
}

variable "aks_privatelink_name" {
  description = "Name to give the private link connection for AKS"
}

variable "aks_hub_vnet_id" {
  description = "id of the vnet in the hub subscription to connect the private link to"
}

variable "aks_privatelink_dns_zone_name" {
  description = "name to give the privatelink zone for aks"
}

variable "aks_privatelink_resource_group" {
  description = "name of the resource group the privatelink zone exists in"
}

variable "aks_version" {
  description = "the version number of the aks cluster to create/upgrade to"
}

data "azurerm_resource_group" "rg" {
  #force this module to wait for rg to be created
  depends_on = [var.resource_group_id]
  name      = var.resource_group_name
}

data "azurerm_private_dns_zone" "k8s_dns_zone" {
  provider            = azurerm.hub
  name                = var.aks_privatelink_dns_zone_name
  resource_group_name = var.aks_privatelink_resource_group
}

resource "azurerm_kubernetes_cluster" "k8s" {
    name                = var.aks_name
    location            = var.azure_location
    resource_group_name = data.azurerm_resource_group.rg.name
    dns_prefix          = var.aks_dns_prefix
    node_resource_group = var.aks_node_resource_group_name
    kubernetes_version  = var.aks_version

    default_node_pool {
      name            = "default"
      node_count      = var.aks_node_count
      vm_size         = var.aks_node_sku
      vnet_subnet_id  = var.aks_vnet_subnet_id
      enable_node_public_ip = "false"
    }

    service_principal {
      client_id     = var.aks_client_id
      client_secret = var.aks_client_secret
    }

    network_profile {
      load_balancer_sku = "Standard"
      network_plugin = "azure"
      network_policy = "azure"
      dns_service_ip = var.aks_dns_service_ip
      docker_bridge_cidr = var.aks_docker_bridge_cidr
      service_cidr = var.aks_service_cidr
      outbound_type = "userDefinedRouting"
    }

    role_based_access_control {
      enabled = "true"
      azure_active_directory {
        managed = "true"
      }
    }

    private_cluster_enabled = "true"
    private_dns_zone_id = data.azurerm_private_dns_zone.k8s_dns_zone.id

    tags      = var.tags
}
